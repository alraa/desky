$(document).ready(function(){
	
	/**
	 * Обработчик размеров окна броузера. Введено
	 * для iOS 8 и выше. Из-за прозрачных баров в Safari
	 * некорректно обрабатывалось значение 100vh.
	 */
	function resizeHandler() {
		if (/iPad|iPhone|iPod/.test(navigator.platform) && !!window.indexedDB) {
			$('.nav').css({ height: window.innerHeight });
		}
	}
	
	/**
	 *		Полноэкранный слайдер	
	 */
	
	$('.js-banner-slider').slick({
		arrows: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		dots: true,
		dotsClass: 'dots__line',
		fade: true,
		infinite: true,
		prevArrow: '<button type="button" class="arrow arrow__circle arrow__prev"><span></span></button>',
		nextArrow: '<button type="button" class="arrow arrow__circle arrow__next"><span></span></button>'
	});
	
	/**
	 *		Слайдер с првеью изображениями	
	 */

	
	var sliders = {
		1: 	{slider : '#slider1', 	nav: '#sliderNav1'},
		2: 	{slider : '#slider2', 	nav: '#sliderNav2'},
		3: 	{slider : '#slider3', 	nav: '#sliderNav3'},
		4: 	{slider : '#slider4', 	nav: '#sliderNav4'},
		5: 	{slider : '#slider5', 	nav: '#sliderNav5'},
		6: 	{slider : '#slider6', 	nav: '#sliderNav6'},
		7: 	{slider : '#slider7', 	nav: '#sliderNav7'},
		8: 	{slider : '#slider8', 	nav: '#sliderNav8'},
		9: 	{slider : '#slider9', 	nav: '#sliderNav9'},
		10: {slider : '#slider10', 	nav: '#sliderNav10'}
	};

	$.each(sliders, function() {

		$(this.slider).slick({
			arrows: true,
			draggable: false,
			fade: false,
			infinite: false,
			prevArrow: '<button type="button" class="arrow arrow__prev"><span></span></button>',
			nextArrow: '<button type="button" class="arrow arrow__next"><span></span></button>',
			slidesToShow: 1,
			slidesToScroll: 1,
			asNavFor: this.nav
		});
		
		$(this.nav).slick({
			arrows: false,
			dots: false,
			draggable: false,
			infinite: false,
			focusOnSelect: true,
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: this.slider,
			responsive: [
				{
					breakpoint: 640,
					settings: {
						slidesToShow: 3
					}
				}
			]
		});

	});
	
	$('.js-prev-slider-list li').click(function(event) {
		event.preventDefault();
		var slideno = $(this).index();
		
		if ($('html').outerWidth() > 640) {
			$('.js-prev-slider-list').slick('slickGoTo', slideno - 1);
		} else {
			$('.js-prev-slider-list').slick('slickGoTo', slideno);
		}
		
	});
	
	/**
	 *		Инициализация галереи	при клике на фото
	 */
	
	$('.js-lightgallery').lightGallery({
		thumbnail: true,
		selector: '.link-gallery'
	});
	
	$('.reviews__slider-item').lightGallery({
		thumbnail: true,
	});
	
	/**
	 *		Слайдер отзывов	
	 */
	
	$('.js-reviews-slider').slick({
		arrows: true,
		dots: false,
		fade: false,
		infinite: false,
		variableWidth: true,
		slidesToShow: 2,
		slidesToScroll: 1,
		prevArrow: '<button type="button" class="arrow arrow__prev"><span></span></button>',
		nextArrow: '<button type="button" class="arrow arrow__next"><span></span></button>',
		responsive: [
				{
					breakpoint: 680,
					settings: {
						slidesToShow: 1,
						variableWidth: false,
						adaptiveHeight: true
					}
				}
			]
	});
	
	/**
	 *		Переключение табов в блоке "Наши работы"	
	 */
	 
	$('.js-tabs-list-button li').on('click', function(e) {
		e.preventDefault();
		
		var indexButton = $(this).index();
		var parentsContainer = $(this).parents('.work__wrapper').find('.js-tabs-list-container > li');
		var selectContainer = $(this).parents('.work__wrapper').find('.js-custom-select');
		
		$('.js-tabs-list-button li').removeClass('is-active');
		$('.js-tabs-list-button li').eq(indexButton).addClass('is-active');
		
		parentsContainer.removeClass('is-active');
		parentsContainer.eq(indexButton).addClass('is-active');
		
		selectContainer.val(indexButton);
	});
	
	$('.js-custom-select').on('change', function(e) {
		e.preventDefault();
		
		var indexButton = $(this).val();
		var parentsContainer = $(this).parents('.work__wrapper').find('.js-tabs-list-container > li');
		
		$('.js-tabs-list-button li').removeClass('is-active');
		$('.js-tabs-list-button li').eq(indexButton).addClass('is-active');
		
		parentsContainer.removeClass('is-active');
		parentsContainer.eq(indexButton).addClass('is-active');
					
		$('.js-prev-slider-list').slick('slickGoTo', 0);
	});
	
	/**
	 *			Фльтры отзывов
	 */

	 
		
	$('.js-filter-list > li').click(function(e) {
		e.preventDefault();
		var buttonFilter = $('.js-filter-list > li');
		var cont = $('.js-reviews-slider');
		
		if (this.id == 'all') {
			cont.slick('slickGoTo', 0);
			cont.slick('slickUnfilter');
		} else {
			cont.slick('slickGoTo', 0);
			cont.slick('slickUnfilter');
			cont.slick('slickFilter', '.' + this.id);
		}
		
		buttonFilter.removeClass('is-active');
		$(this).addClass('is-active');
	});
	
	/**
	 *		Открытие/закрытие гамбургер меню	
	 */
	
	$('.js-open-menu').on('click', function(event) {
		event.preventDefault();
		$('body, .wrapper').addClass('menu-open');
		$('.wrapper-substrate').fadeIn();
	});
	
	$('.js-close-menu').on('click', function(event) {
		event.preventDefault();
		$('body, .wrapper').removeClass('menu-open');
		$('.wrapper-substrate').fadeOut();
	});
	
	/**
	 *		Фиксирование меню при скролле	
	 */
	
	function fixedHeader() {
		var heightPanel = $('.top-panel').outerHeight();
		var scrollBody = $('body').scrollTop();
		var scrollHtml = $('html').scrollTop();

		if ((heightPanel < scrollBody) || (heightPanel < scrollHtml)) {
			$('.js-fixed').addClass('fixed');
		} else { 
			$('.js-fixed').removeClass('fixed'); 
		};
	}
	
	fixedHeader();
	
	$(window).scroll(function() {
		
		if ($('html').outerWidth() > 640) {
			$('.js-prev-slider-list').slick('slickGoTo', 0);
		}
		
		fixedHeader();
	});


	//Слайжер цены

/* слайдер цен */

if($('#slider-price').length){
$("#slider-price").slider({
	min: 1000,
	max: 100000,
	values: [1000,100000],
	range: true,
	step: 1000,
	stop: function(event, ui) {
		$("input#minCost").val($("#slider-price").slider("values",0));
		$("input#maxCost").val($("#slider-price").slider("values",1));
		
    },
    slide: function(event, ui){
		$("input#minCost").val($("#slider-price").slider("values",0));
		$("input#maxCost").val($("#slider-price").slider("values",1));
		$('.price-count__min').text($("#slider-price").slider("values",0));
		$('.price-count__max').text($("#slider-price").slider("values",1));
    }
});

$("input#minCost").change(function(){

	var value1 = $("input#minCost").val();
	var value2 = $("input#maxCost").val();

    if(parseInt(value1) > parseInt(value2)){
		value1 = value2;
		$("input#minCost").val(value1);
	}
	$("#slider-price").slider("values",0,value1);
});

$("input#maxCost").change(function(){
		
	var value1 = $("input#minCost").val();
	var value2 = $("input#maxCost").val();
	if (value2 > 100000) { value2 = 100000; $("input#maxCost").val(10000)}

	if(parseInt(value1) > parseInt(value2)){
		value2 = value1;
		$("input#maxCost").val(value2);
	}
	$("#slider-price").slider("values",1,value2);
});


// фильтрация ввода в поля
	$('input#maxCost, input#minCost').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;
		
		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;
	
		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);
		
		if(!/\d/.test(keyChar))	return false;
	
	});


	$('.price-count__min').text($("#slider-price").slider("values",0));
	$('.price-count__max').text($("#slider-price").slider("values",1));

}

	// маска для ввода номера телефона
	if ($('#phone').length){
		$("#phone").mask("+7(999) 999-99-99");
	}

	$('.js-catalog-filter').on('click', function() {
		$(this).toggleClass('open');
		var dropDown = $(this).next();
		$(dropDown).slideToggle( "slow" );
		return false;
	});


	// Яндекс карты
	if ($('#map').length) {
	ymaps.ready(init);
    var myMap, 
        myPlacemark;

    function init(){ 
      myMap = new ymaps.Map("map", {
          center: [55.795428, 37.745834],
          zoom: 14,
          controls: []
      }); 
      
      myPlacemark = new ymaps.Placemark([55.795428, 37.745834], {
          balloonContent: 'Москва, ул. Советская 10/43'
      }, {
        iconLayout: 'default#image',
        iconImageHref: 'img/marker.png',
        iconImageSize: [41, 56],
        iconImageOffset: [-20, -50]
			});
      
      myMap.geoObjects.add(myPlacemark);
      myMap.behaviors.disable('scrollZoom');
      if (isMobile.any()) {
      	myMap.behaviors.disable('drag');
      }
    }
	}

	// Fancybox as popup
	if ($('.js-popup').length) {
		$("[data-fancybox]").fancybox({});
	}

	// Slider on work page
	if($('.slider-work').length) {
		$('.slider-work').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-work-nav'
		});
		$('.slider-work-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-work',
			focusOnSelect: true
		});
	}

	// Slider on order page
	if($('.slider-order').length) {
		$('.slider-order').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-order-nav'
		});

			$('.slider-order-nav').slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			asNavFor: '.slider-order',
			focusOnSelect: true,
			responsive: [
			  {
			    breakpoint: 910,
			    settings: {
			      slidesToShow: 3
			    }
			  }
			]
		});
		}
	// Maps on mobile
	var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
	};
});